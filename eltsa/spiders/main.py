import scrapy

from eltsa.items import EltsaItem


def get_rowspan(count):
    if count:
        return int(count)
    else:
        return None


class MainSpider(scrapy.Spider):
    name = 'main'
    allowed_domains = ['keltra.eltsa.lt']
    start_urls = ['https://keltra.eltsa.lt/kelappweb/static/pilnas_sarasas.html']

    def parse(self, response):
        companies = response.css('td.body > table tr')

        company_code = None
        for company in companies[1:]:
            rowspan = get_rowspan(company.css('td:first-child::attr(rowspan)').get())
            if rowspan is None:
                # licenses row
                data = company.css('td ::text').extract()
                if data:
                    yield EltsaItem(
                        company_code=company_code,
                        license_type=data[0].strip(),
                        license_number=data[1].strip(),
                        license_status=data[2].strip(),
                        license_copies_num=int(data[7].strip()),
                        valid_from=data[3].strip(),
                        valid_until=data[4].strip(),
                        suspended_from=data[5].strip(),
                        suspended_until=data[6].strip(),
                    )
            else:
                # company row
                columns = company.css('td')
                company_code= columns[1].css('::text').get().split(',')[0]
                yield EltsaItem(
                    company_code= company_code,
                    license_type= columns[4].css('::text').get().strip(),
                    license_number= columns[5].css('::text').get().strip(),
                    license_status= columns[6].css('::text').get().strip(),
                    license_copies_num= int(columns[11].css('::text').get().strip()),
                    valid_from= columns[7].css('::text').get().strip(),
                    valid_until= columns[8].css('::text').get().strip(),
                    suspended_from= columns[9].css('::text').get().strip(),
                    suspended_until= columns[10].css('::text').get().strip(),
                )

