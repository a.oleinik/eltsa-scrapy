import scrapy
from eltsa.items import EltsaItem


class PagesSpider(scrapy.Spider):
    name = 'pages'
    allowed_domains = ['keltra.eltsa.lt']
    start_urls = ['https://keltra.eltsa.lt/kelappweb/static/pilnas_sarasas.html']

    def __init__(self, jsessesionid='64D6F87E12E5A26FBC9374BE2A2B3B8C', *args, **kwargs):
        super(PagesSpider, self).__init__(*args, **kwargs)
        self.jsessesionid = jsessesionid

    def parse(self, response):
        links = response.css('td.body > table tr > td > a::attr(href)').extract()
        for href in links:
            url = response.urljoin(href)
            yield scrapy.Request(url, cookies={'JSESSIONID': self.jsessesionid},
                                 callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        infoTableValue = response.css('form > table:first-child tr td:last-child ::text').extract()
        licensesTableValue = response.css('form table:last-child tr')

        for row in licensesTableValue[1:]:
            columns = row.css('td ::text').extract()
            print(infoTableValue[0].strip())
            yield EltsaItem(
                company_code=infoTableValue[0].strip(),
                license_type=columns[0].strip(),
                license_number=columns[1].strip(),
                license_status=columns[2].strip(),
                license_copies_num=int(columns[7].strip()),
                valid_from=columns[3].strip(),
                valid_until=columns[4].strip(),
                suspended_from=columns[5].strip(),
                suspended_until=columns[6].strip(),
            )
