# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
from dataclasses import dataclass
from datetime import datetime

import scrapy

def serialize_date(value):
    return datetime.strptime(value, '%Y-%m-%d') if value != '-' else None


class EltsaItem(scrapy.Item):
    company_code = scrapy.Field()  # string
    license_type = scrapy.Field()  # string
    license_number = scrapy.Field()  # string
    license_status = scrapy.Field()  # string
    license_copies_num = scrapy.Field()  # int
    valid_from = scrapy.Field(serializer=serialize_date)  # date
    valid_until = scrapy.Field(serializer=serialize_date)  # date
    suspended_from = scrapy.Field(serializer=serialize_date)  # date
    suspended_until = scrapy.Field(serializer=serialize_date)  # date
